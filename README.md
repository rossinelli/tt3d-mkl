# tt3d-mkl : A 3D Trigonometric Transform library

This library sits on top of Intel's MKL Trigonometric Transform (TT) routines to make Type-I 3D Discrete Cosinus Transforms (DCT) on small cubic domains. The existence of the present library is justified by the lack of 3D TT counterparts inside MKL. The C code around the TT 1D routines -- the latter being at the core of this library -- can achieve decent performance at core/hardware-thread level. The code does NOT take care of thread-level parallelism.

## Performance
Representative performance for a computation over 65 x 65 x 65 points. 
Platform: ```Intel(R) Xeon(R) CPU E5-2620 v4 @ 2.10GHz```
Compiled with: ```make ; bin/bench```
Result:
```
TT3D: 1072.75 kB
TT3D: cycles: 8.385e+06
TT3D: MEM BW:  0.13 B/c
```
Compared to FFTW, it is about 50% faster. See ```./bin/bench.c``` for details.

## Library
The current design requires to know the computational problem size at compile time. One can generate the library for a 33 x 33 x 33 transform by compiling the library with ```make -C ./lib n=33```. This will generate a library named ```libtt3d-n33.so```.  As specified in ```./lib/tt3d.h``` the library has two entry points:
```
int tt3d_size1d();
void tt3d(float * inout);
```
The first function enables the runtime identification of the generated library, the second function performs the 3D transform. 

## Extensions
The current code performs just DCT-I transforms for a specific size, known at compile time. However, it should be straightforward to extend the library for other types of DCT or DST (see tt3d.c) and supporting arbitrary problem sizes at runtime (tt3d.c, transpose.c) at the cost of an increase in time-to-solution.


