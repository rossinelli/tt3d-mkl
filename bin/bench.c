#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include <fftw3.h>

#include <tt3d.h>

#define L1REPORT(title)				\
    /* report l1 mass of kernel */		\
    do						\
    {						\
	double s = 0;				\
						\
	for(int i = 0; i < n3; ++i)		\
	    s += kernel[i];			\
						\
	printf("%s: |kernel|1 = %.20e\n",	\
	       title, s);			\
    }						\
    while(0)

#define XYTRANS()					\
    /* xy-transpose */					\
    do							\
    {							\
	float * tmp = malloc(sizeof(float) * n3);	\
							\
	for(int z = 0; z < n; ++z)			\
	    for(int y = 0; y < n; ++y)			\
		for(int x = 0; x < n; ++x)		\
		    tmp[x + n * (y + n * z)] =		\
			kernel[y + n * (x + n * z)];	\
							\
	memcpy(kernel, tmp, sizeof(float) * n3);	\
    							\
	free(tmp);					\
    }							\
    while(0)

#define XZTRANS()					\
    /* xz-transpose */					\
    do							\
    {							\
	float * tmp = malloc(sizeof(float) * n3);	\
							\
	for(int z = 0; z < n; ++z)			\
	    for(int y = 0; y < n; ++y)			\
		for(int x = 0; x < n; ++x)		\
		    tmp[x + n * (y + n * z)] =		\
			kernel[z + n * (y + n * x)];	\
							\
	memcpy(kernel, tmp, sizeof(float) * n3);	\
    							\
	free(tmp);					\
    }							\
    while(0)

enum
{
    n = 65,
    n2 = n * n,
    n3 = n2 * n,
    ntimes = 100
};

void cook_kernel(float * kernel)
{
    const double SIGMA = 0.85;

    const int wcoarse = 32;

    const float freq_cutoff = n * 1.f / wcoarse;
    const float h = freq_cutoff * 2 * M_PI / n;
    const float multiplier = -0.5f * h * h / (SIGMA * SIGMA);

    for(int z = 0; z < n; ++z)
	for(int y = 0; y < n; ++y)
	    for(int x = 0; x < n; ++x)
		kernel[x + n * (y + n * z)] = expf(multiplier * (x * x + y * y + z * z));
}

#include <stdint.h>

uint64_t rdtsc()
{
    unsigned int lo, hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}

void perf_report (
    const char prefix[],
    const uint64_t t0,
    const uint64_t t1)
{
    printf("%s: %.2f kB\n",
	   prefix, sizeof(float) * n3 / 1024.);

    printf("%s: cycles: %.3e\n",
	   prefix, (size_t)(t1 - t0) / (double)ntimes);

    printf("%s: MEM BW: \x1b[91m %.2f B/c\x1b[0m\n",
	   prefix, sizeof(float) * n3  / (double)(t1 - t0) * ntimes);
}

void test_tt3d()
{
    float * kernel = malloc(sizeof(float) * n3);

    cook_kernel(kernel);

    /* data dump in physical space */
    {
	FILE * f = fopen("k.raw", "wb");
	fwrite(kernel, sizeof(float) * n2, n, f);
	fclose(f);
    }

    L1REPORT("TT3D: initial condition");

    if (tt3d_size1d() != n)
    {
	printf("ooops n (%d) is not n of tt3d_n %d\n",
	       n, tt3d_size1d());

	exit(EXIT_FAILURE);
    }

    tt3d(kernel);

    L1REPORT("TT3D: after forward transform");

    /* data dump of transposed transformed kernel */
    {
	FILE * f = fopen("K.raw", "wb");
	fwrite(kernel, sizeof(float) * n2, n, f);
	fclose(f);
    }

    const uint64_t t0 = rdtsc();

    for(int t = 0; t < ntimes; ++t)
	tt3d(kernel);

    const uint64_t t1 = rdtsc();

    free(kernel);

    perf_report("TT3D", t0, t1);
}

void test_fftw()
{
    float * kernel = malloc(sizeof(float) * n3);

    cook_kernel(kernel);

    /* data dump in physical space */
    {
	FILE * f = fopen("k-ref.raw", "wb");
	fwrite(kernel, sizeof(float) * n2, n, f);
	fclose(f);
    }

    L1REPORT("FFTW: initial condition");

    fftwf_plan dctplan = fftwf_plan_r2r_3d(n, n, n, kernel, kernel,
					   FFTW_REDFT00, FFTW_REDFT00, FFTW_REDFT00,
					   FFTW_ESTIMATE);

    if (dctplan == 0)
    {
	printf("oh no! plan is null\n");
	exit(-1);
    }

    fftwf_execute(dctplan);

    L1REPORT("FFTW: after forward transform");

    /* data dump of transposed transformed kernel */
    {
	FILE * f = fopen("K-ref.raw", "wb");
	fwrite(kernel, sizeof(float) * n2, n, f);
	fclose(f);
    }

    const uint64_t t0 = rdtsc();

    for(int t = 0; t < ntimes; ++t)
	fftwf_execute(dctplan);

    const uint64_t t1 = rdtsc();

    fftwf_destroy_plan(dctplan);

    free(kernel);

    perf_report("FFTW", t0, t1);
}

int main()
{
    printf("n = %d -> N = %d\n", n, 2 * n - 2);

    test_tt3d();
    test_fftw();

    return EXIT_SUCCESS;
}
