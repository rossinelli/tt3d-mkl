
FOLDERS = concepts lib bin

all :
	for D in $(FOLDERS) ; do $(MAKE) -C $$D ; done

clean : 
	for D in $(FOLDERS) ; do $(MAKE) -C $$D clean ; done

.PHONY: all clean
