#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include <mkl_trig_transforms.h>

int main()
{
    const int n = 64;
    printf("n = %d -> N = %d\n", n, 2 * n - 2);

    float kernel[n];

    /* cook the kernel */
    {
	const double SIGMA = 0.85;

	const int wcoarse = 32;

	const float freq_cutoff = n * 1.f / wcoarse;
	const float h = freq_cutoff * 2 * M_PI / n;

	const float multiplier = -0.5f * h * h / (SIGMA * SIGMA);

	for(int x = 0; x < n; ++x)
	    kernel[x] = expf(multiplier * (x * x));
    }

    /* report l1 mass of kernel */
    {
	double s = 0;

	for(int i = 0; i < n; ++i)
	    s += kernel[i];

	printf("|kernel|1 = %.20e\n", s);
    }
    
    MKL_INT tt_type = MKL_COSINE_TRANSFORM;
    MKL_INT ipar[128];
    float spar[(5 * n) / 2 + 2];
    MKL_INT stat = 0;
    int nm1 = n - 1;

    s_init_trig_transform(&nm1, &tt_type, ipar, spar, &stat);
    printf("stat = %d\n", stat);

    float dctin[n];
    DFTI_DESCRIPTOR_HANDLE handle;

    s_commit_trig_transform(dctin, &handle, ipar, spar, &stat);
    printf("stat = %d\n", stat);

    memcpy(dctin, kernel, sizeof(kernel));

    s_backward_trig_transform(dctin, &handle, ipar, spar, &stat);
    printf("stat = %d\n", stat);

    free_trig_transform(&handle, ipar, &stat);
    printf("stat = %d\n", stat);

    printf("all good so far\n");

    float dctout[n];
    memcpy(dctout, dctin, sizeof(dctout));
    for(int i = 0; i < n ; ++i)
	dctout[i] *= 2;

    {
	double s = 0;

	for(int i = 0; i < n; ++i)
	    s += dctout[i];

	printf("|kernel|1 = %.20e\n", s);
    }
    
    return EXIT_SUCCESS;
}
