#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include <mkl_trig_transforms.h>

#define L1REPORT(title)				\
    /* report l1 mass of kernel */		\
    do						\
    {						\
	double s = 0;				\
						\
	for(int i = 0; i < n; ++i)		\
	    s += kernel[i];			\
						\
	printf("%s: |kernel|1 = %.20e\n",	\
	       title, s);			\
    }						\
    while(0)

int main()
{
    const int n = 64;
    
    printf("n = %d -> N = %d\n", n, 2 * n - 2);
    
    float __attribute__((aligned(32))) kernel[n];
    
    /* cook the kernel */
    {
	const double SIGMA = 0.85;
	
	const int wcoarse = 32;
	
	const float freq_cutoff = n * 1.f / wcoarse;
	const float h = freq_cutoff * 2 * M_PI / n;
	
	const float multiplier = -0.5f * h * h / (SIGMA * SIGMA);

	for(int x = 0; x < n; ++x)
	    kernel[x] = expf(multiplier * (x * x));
    }
        
    L1REPORT("initial condition");
    
    MKL_INT tt_type = MKL_COSINE_TRANSFORM;
    MKL_INT ipar[128];
    float __attribute__((aligned(32))) spar[(5 * n) / 2 + 2];
    MKL_INT stat = 0;
    int nm1 = n - 1;
    
    s_init_trig_transform(&nm1, &tt_type, ipar, spar, &stat);
    assert(stat == 0);
    
    DFTI_DESCRIPTOR_HANDLE handle;

    s_commit_trig_transform(kernel, &handle, ipar, spar, &stat);
    assert(stat == 0);

    s_forward_trig_transform(kernel, &handle, ipar, spar, &stat);
    assert(stat == 0);

    L1REPORT("after forward transform");

    s_backward_trig_transform(kernel, &handle, ipar, spar, &stat);
    assert(stat == 0);

    free_trig_transform(&handle, ipar, &stat);
    assert(stat == 0);

    L1REPORT("after backward transform");
    
    return EXIT_SUCCESS;
}
