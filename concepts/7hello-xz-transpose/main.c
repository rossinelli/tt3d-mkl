#include <stdio.h>
#include <stdlib.h>

enum
{
    n = 65,
    nnice = n & ~7,
    n2 = n * n,
    n3 = n2 * n,
    TILE = 8,
    TILE2 = TILE * TILE
};

#ifdef _PORTABLE_
#define LOAD_TRANSPOSED(srcbuf, dstbuf)					\
    do									\
    {									\
	for(int j = 0; j < TILE; ++j)					\
	{								\
	    const float * __restrict__ const s = srcbuf + n2 * j;	\
									\
	    float * __restrict__ const d = staging + TILE * j;		\
									\
	    for(int i = 0; i < TILE; ++i)				\
		d[i] = s[i];						\
	}								\
									\
	for(int j = 0; j < TILE; ++j)					\
	    for(int i = 0; i < TILE; ++i)				\
		dstbuf[i + TILE * j] = staging[j + TILE * i];		\
									\
    }									\
    while(0)
#else
#include <immintrin.h>
#define TR8x8(v0, v1, v2, v3, v4, v5, v6, v7)				\
    {									\
	__m256 __t0 = _mm256_unpacklo_ps(v0, v1);			\
	__m256 __t1 = _mm256_unpackhi_ps(v0, v1);			\
	__m256 __t2 = _mm256_unpacklo_ps(v2, v3);			\
	__m256 __t3 = _mm256_unpackhi_ps(v2, v3);			\
	__m256 __t4 = _mm256_unpacklo_ps(v4, v5);			\
	__m256 __t5 = _mm256_unpackhi_ps(v4, v5);			\
	__m256 __t6 = _mm256_unpacklo_ps(v6, v7);			\
	__m256 __t7 = _mm256_unpackhi_ps(v6, v7);			\
        								\
	__m256 __tt0 = _mm256_shuffle_ps(__t0,__t2,_MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt1 = _mm256_shuffle_ps(__t0,__t2,_MM_SHUFFLE(3,2,3,2)); \
	__m256 __tt2 = _mm256_shuffle_ps(__t1,__t3,_MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt3 = _mm256_shuffle_ps(__t1,__t3,_MM_SHUFFLE(3,2,3,2)); \
	__m256 __tt4 = _mm256_shuffle_ps(__t4,__t6,_MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt5 = _mm256_shuffle_ps(__t4,__t6,_MM_SHUFFLE(3,2,3,2)); \
	__m256 __tt6 = _mm256_shuffle_ps(__t5,__t7,_MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt7 = _mm256_shuffle_ps(__t5,__t7,_MM_SHUFFLE(3,2,3,2)); \
                                                                        \
	v0 = _mm256_permute2f128_ps(__tt0, __tt4, 0x20);		\
	v1 = _mm256_permute2f128_ps(__tt1, __tt5, 0x20);		\
	v2 = _mm256_permute2f128_ps(__tt2, __tt6, 0x20);		\
	v3 = _mm256_permute2f128_ps(__tt3, __tt7, 0x20);		\
	v4 = _mm256_permute2f128_ps(__tt0, __tt4, 0x31);		\
	v5 = _mm256_permute2f128_ps(__tt1, __tt5, 0x31);		\
	v6 = _mm256_permute2f128_ps(__tt2, __tt6, 0x31);		\
	v7 = _mm256_permute2f128_ps(__tt3, __tt7, 0x31);		\
    }

#define LOAD_TRANSPOSED(srcbuf, dstbuf)					\
	do								\
	{								\
	    __m256 reg0 = _mm256_loadu_ps(srcbuf + n2 * 0);		\
	    __m256 reg1 = _mm256_loadu_ps(srcbuf + n2 * 1);		\
	    __m256 reg2 = _mm256_loadu_ps(srcbuf + n2 * 2);		\
	    __m256 reg3 = _mm256_loadu_ps(srcbuf + n2 * 3);		\
	    __m256 reg4 = _mm256_loadu_ps(srcbuf + n2 * 4);		\
	    __m256 reg5 = _mm256_loadu_ps(srcbuf + n2 * 5);		\
	    __m256 reg6 = _mm256_loadu_ps(srcbuf + n2 * 6);		\
	    __m256 reg7 = _mm256_loadu_ps(srcbuf + n2 * 7);		\
	    								\
	    TR8x8(reg0, reg1, reg2, reg3, reg4, reg5, reg6, reg7);	\
	    								\
	    _mm256_store_ps(dstbuf + TILE * 0, reg0);			\
	    _mm256_store_ps(dstbuf + TILE * 1, reg1);			\
	    _mm256_store_ps(dstbuf + TILE * 2, reg2);			\
	    _mm256_store_ps(dstbuf + TILE * 3, reg3);			\
	    _mm256_store_ps(dstbuf + TILE * 4, reg4);			\
	    _mm256_store_ps(dstbuf + TILE * 5, reg5);			\
	    _mm256_store_ps(dstbuf + TILE * 6, reg6);			\
	    _mm256_store_ps(dstbuf + TILE * 7, reg7);			\
	}								\
	while(0)
#endif

#ifdef _PORTABLE_
#define WRITE(srcbuf, dstbuf)						\
    do									\
    {									\
	for(int j = 0; j < TILE; ++j)					\
	{								\
	    const float * __restrict__ const s = srcbuf + TILE * j;	\
	    float * __restrict__ const d = dstbuf + n2 * j;		\
									\
	    for(int i = 0; i < TILE; ++i)				\
		d[i] = s[i];						\
	}								\
    }									\
    while(0)
#else
#define WRITE(srcbuf, dstbuf)						\
    do									\
    {									\
	_mm256_storeu_ps(dstbuf + n2 * 0, _mm256_load_ps(srcbuf + TILE * 0)); \
	_mm256_storeu_ps(dstbuf + n2 * 1, _mm256_load_ps(srcbuf + TILE * 1)); \
	_mm256_storeu_ps(dstbuf + n2 * 2, _mm256_load_ps(srcbuf + TILE * 2)); \
	_mm256_storeu_ps(dstbuf + n2 * 3, _mm256_load_ps(srcbuf + TILE * 3)); \
	_mm256_storeu_ps(dstbuf + n2 * 4, _mm256_load_ps(srcbuf + TILE * 4)); \
	_mm256_storeu_ps(dstbuf + n2 * 5, _mm256_load_ps(srcbuf + TILE * 5)); \
	_mm256_storeu_ps(dstbuf + n2 * 6, _mm256_load_ps(srcbuf + TILE * 6)); \
	_mm256_storeu_ps(dstbuf + n2 * 7, _mm256_load_ps(srcbuf + TILE * 7)); \
    }									\
    while(0)
#endif

void xz_transpose(float * __restrict__ const data)
{
    float __attribute__((aligned(64))) tmp0[TILE2], tmp1[TILE2]
#ifdef _PORTABLE_
	, staging[TILE2]
#endif
	;

    for(int z = 0; z < nnice; z += TILE)
    {
	float * __restrict__ const base0 = data + n2 * z;
	float * __restrict__ const base1 = data + z;

	for(int y = 0; y < n; ++y)
	{
	    float * __restrict__ const r0 = base0 + z + n * y;

	    LOAD_TRANSPOSED(r0, tmp0);

	    WRITE(tmp0, r0);
	}

	for(int x = z + TILE; x < nnice; x += TILE)
	{
	    float * __restrict__ const r0 = base0 + x;
	    float * __restrict__ const r1 = base1 + n2 * x;

	    for(int y = 0; y < n; ++y)
	    {
		float * __restrict__ const t0 = r0 + n * y;
		float * __restrict__ const t1 = r1 + n * y;

		LOAD_TRANSPOSED(t0, tmp0);
		LOAD_TRANSPOSED(t1, tmp1);

		WRITE(tmp0, t1);
		WRITE(tmp1, t0);
	    }
	}
    }

    for(int z = nnice; z < n; ++z)
	for(int x = 0; x < z; ++x)
	    for(int y = 0; y < n; ++y)
	    {
		const float t0 = data[x + n * (y + n * z)];
		const float t1 = data[z + n * (y + n * x)];
		
		data[x + n * (y + n * z)] = t1;
		data[z + n * (y + n * x)] = t0;
	    }
}

#include <stdint.h>

uint64_t rdtsc()
{
    unsigned int lo, hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}

#include <string.h>

void xz_gold (
    float * const data,
    float * const tmp)
{
    for(int zi = 0; zi < n; ++zi)
	for(int yi = 0; yi < n; ++yi)
	    for(int xi = 0; xi < n; ++xi)
		tmp[xi + n * (yi + n * zi)] = data[zi + n * (yi + n * xi)];

    memcpy(data, tmp, sizeof(float) * n3);
}

float ic[n3], ref[n3], res[n3], tmp[n3];

int main()
{
    for(int i = 0; i < n3; ++i)
	ic[i] = i;

    const int ntimes = ~1 & (int)1e3;

    memcpy(res, ic, sizeof(ic));
    memcpy(ref, ic, sizeof(ic));

    xz_transpose(res);

    xz_gold(ref, tmp);

    if (memcmp(ref, res, sizeof(ic)) != 0)
    {
	printf("\x1b[41mBAD!\x1b[0m\n");

	return EXIT_FAILURE;
    }
    else
	printf("\x1b[92mCORRECTNESS TEST PASSED\x1b[0m\n");

    const uint64_t t0 = rdtsc();

    for(int i = 0; i < ntimes; ++i)
	xz_gold(ref, tmp);

    const uint64_t t1 = rdtsc();

    for(int i = 0; i < ntimes; ++i)
	xz_transpose(ref);

    const uint64_t t2 = rdtsc();

    printf("XZ-TRANSPOSE: %.2f kB\n",
	   sizeof(ic) / 1024.);

    printf("XZ-TRANSPOSE: gold cycles: %.3e\n",
	   (size_t)(t1 - t0) / (double)ntimes);

    printf("XZ-TRANSPOSE: cycles: %.3e MEM BW: \x1b[91m %.2f B/c\x1b[0m\n",
	   (size_t)(t2 - t1) / (double)ntimes,
	   sizeof(ic)  / (double)(t2 - t1) * ntimes );

    printf("XZ-TRANSPOSE: improvement: \x1b[92m%.2fX\x1b[0m\n",
	   (t1 - t0) / (double)(t2 - t1));

    return EXIT_SUCCESS;
}
