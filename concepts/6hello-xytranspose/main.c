#include <stdio.h>
#include <stdlib.h>

enum { n = 65, n2 = n * n, TILE = 8, TILE2 = TILE * TILE };

#ifdef _PORTABLE_
#define LOAD_TRANSPOSED(srcbuf, dstbuf)					\
    do									\
    {									\
	for(int j = 0; j < TILE; ++j)					\
	{								\
	    const float * __restrict__ const s = srcbuf + n * j;	\
	    float * __restrict__ const d = staging + TILE * j;		\
	    								\
	    for(int i = 0; i < TILE; ++i)				\
		d[i] = s[i];						\
	}								\
									\
	for(int j = 0; j < TILE; ++j)					\
	    for(int i = 0; i < TILE; ++i)				\
		dstbuf[i + TILE * j] = staging[j + TILE * i];		\
									\
    }									\
    while(0)
#else
#include <immintrin.h>
#define TR8x8(v0, v1, v2, v3, v4, v5, v6, v7)				\
    {									\
	__m256 __t0 = _mm256_unpacklo_ps(v0, v1);			\
	__m256 __t1 = _mm256_unpackhi_ps(v0, v1);			\
	__m256 __t2 = _mm256_unpacklo_ps(v2, v3);			\
	__m256 __t3 = _mm256_unpackhi_ps(v2, v3);			\
	__m256 __t4 = _mm256_unpacklo_ps(v4, v5);			\
	__m256 __t5 = _mm256_unpackhi_ps(v4, v5);			\
	__m256 __t6 = _mm256_unpacklo_ps(v6, v7);			\
	__m256 __t7 = _mm256_unpackhi_ps(v6, v7);			\
                                                                        \
	__m256 __tt0 = _mm256_shuffle_ps(__t0,__t2,_MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt1 = _mm256_shuffle_ps(__t0,__t2,_MM_SHUFFLE(3,2,3,2)); \
	__m256 __tt2 = _mm256_shuffle_ps(__t1,__t3,_MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt3 = _mm256_shuffle_ps(__t1,__t3,_MM_SHUFFLE(3,2,3,2)); \
	__m256 __tt4 = _mm256_shuffle_ps(__t4,__t6,_MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt5 = _mm256_shuffle_ps(__t4,__t6,_MM_SHUFFLE(3,2,3,2)); \
	__m256 __tt6 = _mm256_shuffle_ps(__t5,__t7,_MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt7 = _mm256_shuffle_ps(__t5,__t7,_MM_SHUFFLE(3,2,3,2)); \
                                                                        \
	v0 = _mm256_permute2f128_ps(__tt0, __tt4, 0x20);		\
	v1 = _mm256_permute2f128_ps(__tt1, __tt5, 0x20);		\
	v2 = _mm256_permute2f128_ps(__tt2, __tt6, 0x20);		\
	v3 = _mm256_permute2f128_ps(__tt3, __tt7, 0x20);		\
	v4 = _mm256_permute2f128_ps(__tt0, __tt4, 0x31);		\
	v5 = _mm256_permute2f128_ps(__tt1, __tt5, 0x31);		\
	v6 = _mm256_permute2f128_ps(__tt2, __tt6, 0x31);		\
	v7 = _mm256_permute2f128_ps(__tt3, __tt7, 0x31);		\
    }

#define LOAD_TRANSPOSED(srcbuf, dstbuf)					\
	do								\
	{								\
	    __m256 reg0 = _mm256_loadu_ps(srcbuf + n * 0);		\
	    __m256 reg1 = _mm256_loadu_ps(srcbuf + n * 1);		\
	    __m256 reg2 = _mm256_loadu_ps(srcbuf + n * 2);		\
	    __m256 reg3 = _mm256_loadu_ps(srcbuf + n * 3);		\
	    __m256 reg4 = _mm256_loadu_ps(srcbuf + n * 4);		\
	    __m256 reg5 = _mm256_loadu_ps(srcbuf + n * 5);		\
	    __m256 reg6 = _mm256_loadu_ps(srcbuf + n * 6);		\
	    __m256 reg7 = _mm256_loadu_ps(srcbuf + n * 7);		\
	    								\
	    TR8x8(reg0, reg1, reg2, reg3, reg4, reg5, reg6, reg7);	\
	    								\
	    _mm256_store_ps(dstbuf + TILE * 0, reg0);			\
	    _mm256_store_ps(dstbuf + TILE * 1, reg1);			\
	    _mm256_store_ps(dstbuf + TILE * 2, reg2);			\
	    _mm256_store_ps(dstbuf + TILE * 3, reg3);			\
	    _mm256_store_ps(dstbuf + TILE * 4, reg4);			\
	    _mm256_store_ps(dstbuf + TILE * 5, reg5);			\
	    _mm256_store_ps(dstbuf + TILE * 6, reg6);			\
	    _mm256_store_ps(dstbuf + TILE * 7, reg7);			\
	}								\
	while(0)
#endif

#ifdef _PORTABLE_
#define WRITE(srcbuf, dstbuf)						\
    do									\
    {									\
	for(int j = 0; j < TILE; ++j)					\
	{								\
	    const float * __restrict__ const s = srcbuf + TILE * j;	\
	    float * __restrict__ const d = dstbuf + n * j;		\
									\
	    for(int i = 0; i < TILE; ++i)				\
		d[i] = s[i];						\
	}								\
    }									\
    while(0)
#else
#define WRITE(srcbuf, dstbuf)						\
    do									\
    {									\
	_mm256_storeu_ps(dstbuf + n * 0, _mm256_load_ps(srcbuf + TILE * 0)); \
	_mm256_storeu_ps(dstbuf + n * 1, _mm256_load_ps(srcbuf + TILE * 1)); \
	_mm256_storeu_ps(dstbuf + n * 2, _mm256_load_ps(srcbuf + TILE * 2)); \
	_mm256_storeu_ps(dstbuf + n * 3, _mm256_load_ps(srcbuf + TILE * 3)); \
	_mm256_storeu_ps(dstbuf + n * 4, _mm256_load_ps(srcbuf + TILE * 4)); \
	_mm256_storeu_ps(dstbuf + n * 5, _mm256_load_ps(srcbuf + TILE * 5)); \
	_mm256_storeu_ps(dstbuf + n * 6, _mm256_load_ps(srcbuf + TILE * 6)); \
	_mm256_storeu_ps(dstbuf + n * 7, _mm256_load_ps(srcbuf + TILE * 7)); \
    }									\
    while(0)
#endif

enum { nnice = n & ~7 } ;

void xy_transpose(float * __restrict__ const data)
{
    float __attribute__((aligned(64))) tmp0[TILE2], tmp1[TILE2]
#ifdef _PORTABLE_
	, staging[TILE2]
#endif
	;

    for(int y = 0; y < nnice; y += TILE)
    {
	{
	    float * __restrict__ const r0 = data + y + n * y;

	    LOAD_TRANSPOSED(r0, tmp0);
	    WRITE(tmp0, r0);
	}

	float * __restrict__ const base0 = data + n * y;
	float * __restrict__ const base1 = data + y;

	for(int x = y + TILE; x < nnice; x += TILE)
	{
	    float * __restrict__ const r0 = base0 + x;
	    float * __restrict__ const r1 = base1 + n * x;

	    LOAD_TRANSPOSED(r0, tmp0);
	    LOAD_TRANSPOSED(r1, tmp1);
	    WRITE(tmp0, r1);
	    WRITE(tmp1, r0);
	}
    }

    for(int y = nnice; y < n; ++y)
	for(int x = 0; x < y; ++x)
	{
	    const float t0 = data[x + n * y];
	    const float t1 = data[y + n * x];

	    data[x + n * y] = t1;
	    data[y + n * x] = t0;
	}
}

#include <stdint.h>

uint64_t rdtsc()
{
    unsigned int lo, hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}

#include <string.h>

void xy_gold (
    float * const data,
    float * const tmp)
{
    for(int yi = 0; yi < n; ++yi)
	for(int xi = 0; xi < n; ++xi)
	    tmp[xi + n * yi] = data[yi + n * xi];

    memcpy(data, tmp, sizeof(float) * n2);
}

float ic[n2], ref[n2], res[n2], tmp[n2];

int main()
{
    for(int i = 0; i < n2; ++i)
	ic[i] = i;

    const int ntimes = ~1 & (int)1e4;

    memcpy(res, ic, sizeof(ic));
    memcpy(ref, ic, sizeof(ic));

    xy_transpose(res);

    xy_gold(ref, tmp);

    if (memcmp(ref, res, sizeof(ic)) != 0)
    {
	printf("\x1b[41mBAD!\x1b[0m\n");

	return EXIT_FAILURE;
    }

    const uint64_t t0 = rdtsc();

    for(int i = 0; i < ntimes; ++i)
	xy_gold(ref, tmp);

    const uint64_t t1 = rdtsc();

    for(int i = 0; i < ntimes; ++i)
	xy_transpose(ref);

    const uint64_t t2 = rdtsc();

    printf("XY-TRANSPOSE: %.2f kB\n",
	   sizeof(ic) / 1024.);

    printf("XY-TRANSPOSE: gold cycles: %.3e\n",
	   (size_t)(t1 - t0) / (double)ntimes);

    printf("XY-TRANSPOSE: cycles: %.3e MEM BW: \x1b[91m %.2f B/c\x1b[0m\n",
	   (size_t)(t2 - t1) / (double)ntimes,
	   sizeof(ic)  / (double)(t2 - t1) * ntimes );

    printf("XY-TRANSPOSE: improvement: \x1b[92m%.2fX\x1b[0m\n",
	   (t1 - t0) / (double)(t2 - t1));

    return EXIT_SUCCESS;
}
