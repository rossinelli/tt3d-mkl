#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include <fftw3.h>
#include <mkl_trig_transforms.h>

#define L1REPORT(title)				\
    /* report l1 mass of kernel */		\
    do						\
    {						\
	double s = 0;				\
						\
	for(int i = 0; i < n2; ++i)		\
	    s += kernel[i];			\
						\
	printf("%s: |kernel|1 = %.20e\n",	\
	       title, s);			\
    }						\
    while(0)

#define XYTRANS()					\
    /* xy-transpose */					\
    do							\
    {							\
	float * tmp = malloc(sizeof(float) * n2);	\
    							\
	for(int y = 0; y < n; ++y)			\
	    for(int x = 0; x < n; ++x)			\
		tmp[x + n * y] = kernel[y + n * x];	\
							\
	memcpy(kernel, tmp, sizeof(float) * n2);	\
    							\
	free(tmp);					\
    }							\
    while(0)

enum { n = 64, n2 = n * n };

void cook_kernel(float * kernel)
{
    const double SIGMA = 0.85;

    const int wcoarse = 32;

    const float freq_cutoff = n * 1.f / wcoarse;
    const float h = freq_cutoff * 2 * M_PI / n;
    const float multiplier = -0.5f * h * h / (SIGMA * SIGMA);

    for(int y = 0; y < n; ++y)
	for(int x = 0; x < n; ++x)
	    kernel[x + n * y] = expf(multiplier * (x * x + y * y));
}

void test_mkl()
{
   float * kernel = malloc(sizeof(float) * n2);

   cook_kernel(kernel);

    /* data dump in physical space */
    {
	FILE * f = fopen("k.raw", "wb");
	fwrite(kernel, sizeof(float) * n, n, f);
	fclose(f);
    }

    L1REPORT("initial condition");

    MKL_INT tt_type = MKL_COSINE_TRANSFORM;
    MKL_INT ipar[128];
    float __attribute__((aligned(32))) spar[(5 * n) / 2 + 2];
    MKL_INT stat = 0;
    int nm1 = n - 1;

    s_init_trig_transform(&nm1, &tt_type, ipar, spar, &stat);
    assert(stat == 0);

    DFTI_DESCRIPTOR_HANDLE handle;

    s_commit_trig_transform(kernel, &handle, ipar, spar, &stat);
    assert(stat == 0);

    /* x-transform */
    for(int y = 0; y < n; ++y)
	s_backward_trig_transform(kernel + n * y, &handle, ipar, spar, &stat);

    XYTRANS();

    /* y-transform */
    for(int y = 0; y < n; ++y)
	s_backward_trig_transform(kernel + n * y, &handle, ipar, spar, &stat);

    XYTRANS();

    L1REPORT("after forward transform");

    /* data dump of transposed transformed kernel */
    {
	FILE * f = fopen("K.raw", "wb");
	fwrite(kernel, sizeof(float) * n, n, f);
	fclose(f);
    }
    assert(stat == 0);

    free_trig_transform(&handle, ipar, &stat);

    free(kernel);
}

void test_fftw()
{
   float * kernel = malloc(sizeof(float) * n2);

   cook_kernel(kernel);

    /* data dump in physical space */
    {
	FILE * f = fopen("k-ref.raw", "wb");
	fwrite(kernel, sizeof(float) * n, n, f);
	fclose(f);
    }

    L1REPORT("FFTW: initial condition");

    fftwf_plan dctplan = fftwf_plan_r2r_2d(n, n, kernel, kernel,
					   FFTW_REDFT00, FFTW_REDFT00, FFTW_ESTIMATE);

    if (dctplan == 0)
    {
	printf("oh no! plan is null\n");
	exit(-1);
    }

    fftwf_execute(dctplan);

    for(int i = 0; i < n2; ++i)
	kernel[i] *= 0.25;

    L1REPORT("FFTW: after forward transform");

    /* data dump of transposed transformed kernel */
    {
	FILE * f = fopen("K-ref.raw", "wb");
	fwrite(kernel, sizeof(float) * n, n, f);
	fclose(f);
    }

    fftwf_destroy_plan(dctplan);

    free(kernel);
}

int main()
{
    printf("n = %d -> N = %d\n", n, 2 * n - 2);

    test_mkl();
    test_fftw();

    return EXIT_SUCCESS;
}
