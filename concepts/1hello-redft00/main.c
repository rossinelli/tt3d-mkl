#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include <fftw3.h>

int main()
{
    const int n = 64;
    printf("n = %d -> N = %d\n", n, 2 * n - 2);

    float * kernel = fftwf_alloc_real(n);

    /* cook the kernel */
    {
	const double SIGMA = 0.85;

	const int wcoarse = 32;

	const float freq_cutoff = n * 1.f / wcoarse;
	const float h = freq_cutoff * 2 * M_PI / n;

	const float multiplier = -0.5f * h * h / (SIGMA * SIGMA);

	for(int x = 0; x < n; ++x)
	    kernel[x] = expf(multiplier * (x * x));
    }

    /* report l1 mass of kernel */
    {
	double s = 0;

	for(int i = 0; i < n; ++i)
	    s += kernel[i];

	printf("|kernel|1 = %.20e\n", s);
    }

    float * dctin = NULL, * dctout = NULL;
    fftwf_plan dctplan = 0;

    dctin = fftwf_alloc_real(n);
    dctout = fftwf_alloc_real(n);

    dctplan = fftwf_plan_r2r_1d(n, dctin, dctout, FFTW_REDFT00, FFTW_MEASURE);

    if (dctplan == NULL)
    {
	printf("oh no, the plan is NULL\n");
	exit(-1);
    }

    memcpy(dctin, kernel, sizeof(float) * n);

    printf("about execute plan\n");

    fftwf_execute(dctplan);

    /* report l1 mass of dctout */
    {
	double s = 0;

	for(int i = 0; i < n; ++i)
	    s += dctout[i];

	printf("|kernel|1 = %.20e\n", s);
    }

    fftwf_destroy_plan(dctplan);

    fftwf_free(dctout);
    fftwf_free(dctin);

    fftwf_free(kernel);

    return EXIT_SUCCESS;
}
