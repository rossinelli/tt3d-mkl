#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

    int tt3d_size1d();

    void tt3d(float * inout);

#ifdef __cplusplus
}
#endif
