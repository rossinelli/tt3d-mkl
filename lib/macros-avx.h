#pragma once

enum { TILE = 8, TILE2 = TILE * TILE };

#include <immintrin.h>

#define TR8x8(v0, v1, v2, v3, v4, v5, v6, v7)			\
    do								\
    {								\
	__m256 __t0 = _mm256_unpacklo_ps(v0, v1);		\
	__m256 __t1 = _mm256_unpackhi_ps(v0, v1);		\
	__m256 __t2 = _mm256_unpacklo_ps(v2, v3);		\
	__m256 __t3 = _mm256_unpackhi_ps(v2, v3);		\
	__m256 __t4 = _mm256_unpacklo_ps(v4, v5);		\
	__m256 __t5 = _mm256_unpackhi_ps(v4, v5);		\
	__m256 __t6 = _mm256_unpacklo_ps(v6, v7);		\
	__m256 __t7 = _mm256_unpackhi_ps(v6, v7);		\
								\
	__m256 __tt0 = _mm256_shuffle_ps(__t0,__t2,		\
					 _MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt1 = _mm256_shuffle_ps(__t0,__t2,		\
					 _MM_SHUFFLE(3,2,3,2)); \
	__m256 __tt2 = _mm256_shuffle_ps(__t1,__t3,		\
					 _MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt3 = _mm256_shuffle_ps(__t1,__t3,		\
					 _MM_SHUFFLE(3,2,3,2)); \
	__m256 __tt4 = _mm256_shuffle_ps(__t4,__t6,		\
					 _MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt5 = _mm256_shuffle_ps(__t4,__t6,		\
					 _MM_SHUFFLE(3,2,3,2)); \
	__m256 __tt6 = _mm256_shuffle_ps(__t5,__t7,		\
					 _MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt7 = _mm256_shuffle_ps(__t5,__t7,		\
					 _MM_SHUFFLE(3,2,3,2)); \
								\
	v0 = _mm256_permute2f128_ps(__tt0, __tt4, 0x20);	\
	v1 = _mm256_permute2f128_ps(__tt1, __tt5, 0x20);	\
	v2 = _mm256_permute2f128_ps(__tt2, __tt6, 0x20);	\
	v3 = _mm256_permute2f128_ps(__tt3, __tt7, 0x20);	\
	v4 = _mm256_permute2f128_ps(__tt0, __tt4, 0x31);	\
	v5 = _mm256_permute2f128_ps(__tt1, __tt5, 0x31);	\
	v6 = _mm256_permute2f128_ps(__tt2, __tt6, 0x31);	\
	v7 = _mm256_permute2f128_ps(__tt3, __tt7, 0x31);	\
    }								\
    while(0)

#define LOAD_TRANSPOSED(in, out, STRIDE)			\
    do								\
    {								\
	__m256 reg0 = _mm256_loadu_ps(in + STRIDE * 0);		\
	__m256 reg1 = _mm256_loadu_ps(in + STRIDE * 1);		\
	__m256 reg2 = _mm256_loadu_ps(in + STRIDE * 2);		\
	__m256 reg3 = _mm256_loadu_ps(in + STRIDE * 3);		\
	__m256 reg4 = _mm256_loadu_ps(in + STRIDE * 4);		\
	__m256 reg5 = _mm256_loadu_ps(in + STRIDE * 5);		\
	__m256 reg6 = _mm256_loadu_ps(in + STRIDE * 6);		\
	__m256 reg7 = _mm256_loadu_ps(in + STRIDE * 7);		\
								\
	TR8x8(reg0, reg1, reg2, reg3, reg4, reg5, reg6, reg7);	\
								\
	_mm256_store_ps(out + 8 * 0, reg0);			\
	_mm256_store_ps(out + 8 * 1, reg1);			\
	_mm256_store_ps(out + 8 * 2, reg2);			\
	_mm256_store_ps(out + 8 * 3, reg3);			\
	_mm256_store_ps(out + 8 * 4, reg4);			\
	_mm256_store_ps(out + 8 * 5, reg5);			\
	_mm256_store_ps(out + 8 * 6, reg6);			\
	_mm256_store_ps(out + 8 * 7, reg7);			\
    }								\
    while(0)

#define WRITE(in, out, STRIDE)					\
	do							\
	{							\
	    _mm256_storeu_ps(out + STRIDE * 0,			\
			    _mm256_load_ps(in + 8 * 0));	\
	    _mm256_storeu_ps(out + STRIDE * 1,			\
			    _mm256_load_ps(in + 8 * 1));	\
	    _mm256_storeu_ps(out + STRIDE * 2,			\
			    _mm256_load_ps(in + 8 * 2));	\
	    _mm256_storeu_ps(out + STRIDE * 3,			\
			    _mm256_load_ps(in + 8 * 3));	\
	    _mm256_storeu_ps(out + STRIDE * 4,			\
			    _mm256_load_ps(in + 8 * 4));	\
	    _mm256_storeu_ps(out + STRIDE * 5,			\
			    _mm256_load_ps(in + 8 * 5));	\
	    _mm256_storeu_ps(out + STRIDE * 6,			\
			    _mm256_load_ps(in + 8 * 6));	\
	    _mm256_storeu_ps(out + STRIDE * 7,			\
			    _mm256_load_ps(in + 8 * 7));	\
	}							\
	while(0)
