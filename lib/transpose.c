#ifdef _AVX_
#include "macros-avx.h"
#else
#include "macros.h"
#endif

enum { N = _N_, NNICE = N & ~7, N2 = N * N };

void transpose_xy (float * __restrict__ const data)
{
    float __attribute__((aligned(64))) tmp0[TILE2], tmp1[TILE2];

    for(int y = 0; y < NNICE; y += TILE)
    {
	{
	    float * __restrict__ const r0 = data + y + N * y;

	    LOAD_TRANSPOSED(r0, tmp0, N);

	    WRITE(tmp0, r0, N);
	}

	float * __restrict__ const base0 = data + N * y;
	float * __restrict__ const base1 = data + y;

	for(int x = y + TILE; x < NNICE; x += TILE)
	{
	    float * __restrict__ const r0 = base0 + x;
	    float * __restrict__ const r1 = base1 + N * x;

	    LOAD_TRANSPOSED(r0, tmp0, N);
	    LOAD_TRANSPOSED(r1, tmp1, N);

	    WRITE(tmp0, r1, N);
	    WRITE(tmp1, r0, N);
	}
    }

    /* handling arbitrary N */
    for(int y = NNICE; y < N; ++y)
        for(int x = 0; x < y; ++x)
        {
            const float t0 = data[x + N * y];
            const float t1 = data[y + N * x];

            data[x + N * y] = t1;
            data[y + N * x] = t0;
        }
}

void transpose_xz (float * __restrict__ const data)
{
    float __attribute__((aligned(64))) tmp0[TILE2], tmp1[TILE2];

    for(int z = 0; z < NNICE; z += TILE)
    {
	float * __restrict__ const base0 = data + N2 * z;
	float * __restrict__ const base1 = data + z;

	for(int y = 0; y < N; ++y)
	{
	    float * __restrict__ const r0 = base0 + z + N * y;

	    LOAD_TRANSPOSED(r0, tmp0, N2);

	    WRITE(tmp0, r0, N2);
	}

	for(int x = z + TILE; x < NNICE; x += TILE)
	{
	    float * __restrict__ const r0 = base0 + x;
	    float * __restrict__ const r1 = base1 + N2 * x;

	    for(int y = 0; y < N; ++y)
	    {
		float * __restrict__ const t0 = r0 + N * y;
		float * __restrict__ const t1 = r1 + N * y;

		LOAD_TRANSPOSED(t0, tmp0, N2);
		LOAD_TRANSPOSED(t1, tmp1, N2);

		WRITE(tmp0, t1, N2);
		WRITE(tmp1, t0, N2);
	    }
	}
    }

    /* handling arbitrary N */
    for(int z = NNICE; z < N; ++z)
        for(int x = 0; x < z; ++x)
            for(int y = 0; y < N; ++y)
            {
                const float t0 = data[x + N * (y + N * z)];
                const float t1 = data[z + N * (y + N * x)];

                data[x + N * (y + N * z)] = t1;
                data[z + N * (y + N * x)] = t0;
            }
}
