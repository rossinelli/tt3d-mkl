#pragma once

enum { TILE = 8, TILE2 = TILE * TILE };

#define LOAD_TRANSPOSED(in, out, STRIDE)			\
    do								\
    {								\
	float __attribute__((aligned(64))) staging[TILE2];	\
								\
	for(int j = 0; j < TILE; ++j)				\
	{							\
	    const float * __restrict__ const s =		\
		in + STRIDE * j;					\
								\
	    float * __restrict__ const d =			\
		staging + TILE * j;				\
								\
	    for(int i = 0; i < TILE; ++i)			\
		d[i] = s[i];					\
	}							\
								\
	for(int j = 0; j < TILE; ++j)				\
	    for(int i = 0; i < TILE; ++i)			\
		out[i + TILE * j] = staging[j + TILE * i];	\
								\
    }								\
    while(0)

#define WRITE(in, out, STRIDE)					\
    do								\
    {								\
	for(int j = 0; j < TILE; ++j)				\
	{							\
	    const float * __restrict__ const s = in + TILE * j;	\
	    float * __restrict__ const d = out + STRIDE * j;	\
								\
	    for(int i = 0; i < TILE; ++i)			\
		d[i] = s[i];					\
	}							\
    }								\
    while(0)
