#include <assert.h>

#include <mkl_trig_transforms.h>

enum { N = _N_, N2 = N * N, N3 = N2 * N };

void transpose_xy (float *);
void transpose_xz (float *);

DFTI_DESCRIPTOR_HANDLE handle;
MKL_INT tt_type = MKL_COSINE_TRANSFORM;

MKL_INT ipar[128];

float __attribute__((aligned(64))) spar[(5 * N) / 2 + 2];

MKL_INT stat = 0;

static void  __attribute__((constructor)) init()
{
    int nm1 = N - 1;
    s_init_trig_transform(&nm1, &tt_type, ipar, spar, &stat);
    assert(stat == 0);
}

__attribute__ ((visibility ("default")))
int tt3d_size1d() { return N; }

__attribute__ ((visibility ("default")))
void tt3d(float * const inout)
{
    s_commit_trig_transform(inout, &handle, ipar, spar, &stat);
    assert(stat == 0);

    for(int z = 0; z < N; ++z)
    {
	float * const __restrict__ slice = inout + N2 * z;

	/* x-transform */
	for(int y = 0; y < N; ++y)
	    s_backward_trig_transform(slice + N * y, &handle, ipar, spar, &stat);

	transpose_xy(slice);

	/* y-transform */
	for(int y = 0; y < N; ++y)
	    s_backward_trig_transform(slice + N * y, &handle, ipar, spar, &stat);

	transpose_xy(slice);
    }

    transpose_xz(inout);

    /* z-transform */
    for(int z = 0; z < N3; z += N)
    {
	float * __restrict__ const line = inout + z;

	s_backward_trig_transform(line, &handle, ipar, spar, &stat);

	for(int i = 0; i < N; ++i)
	    line[i] *= 8.f;
    }

    transpose_xz(inout);
}

static void __attribute__((destructor)) dispose()
{
    free_trig_transform(&handle, ipar, &stat);
}
